#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

enum token_kind {
    TOKEN_PLUS = 0,
    TOKEN_MINUS,
    TOKEN_ASTERISK,
    TOKEN_SLASH,
    TOKEN_LEFT_BRACE,
    TOKEN_RIGHT_BRACE,

    TOKEN_NUMBER,

    TOKEN_EOF
};

static const char *const KIND2STR[] = {
    "PLUS",
    "MINUS",
    "ASTERISK",
    "SLASH",
    "LEFT BRACE",
    "RIGHT BRACE",
    "NUMBER",
    "EOF"
};

struct token {
    enum token_kind kind;
    // TODO: add location
    char* start;
    size_t len;
};

struct parser {
    char* text;
    int len;
    int position;
};

void parser_new(struct parser *p, char *text) {
    p->text = text;
    p->len = strlen(text);
    p->position = 0;
}

char parser_peek(struct parser *p, int off) {
    const int pos = p->position + off;
    return 0 <= pos && pos <= p->len ? p->text[pos] : '\0';
}

char parser_current(struct parser *p) {
    return parser_peek(p, 0);
}

void parser_next(struct parser *p) {
    ++p->position;
}

#define INIT_TOKEN(t, kind_, pos_, len_)                                       \
    do {                                                                       \
        t->kind = kind_;                                                       \
        t->start = pos_;                                                       \
        t->len = len_;                                                         \
    } while(0)

int isalpha(char c) {
    return c >= '0' && c <= '9';
}

void parse_number(struct parser *p, struct token *tok) {
    int size = 0;
    while (isalpha(parser_peek(p, size))) ++size;
    INIT_TOKEN(tok, TOKEN_NUMBER, p->text + p->position, size);
    p->position += size - 1;
}

void parser_lex(struct parser *p, struct token *tok) {
    while (parser_current(p) == ' ') ++p->position;
    switch (parser_current(p)) {
    case '\0':
        INIT_TOKEN(tok, TOKEN_EOF, p->text + p->position, 1);
        break;
    case '0': case '1': case '2': case '3': case '4':
    case '5': case '6': case '7': case '8': case '9':
        parse_number(p, tok);
        break;
    case '+': INIT_TOKEN(tok, TOKEN_PLUS, p->text + p->position, 1); break;
    case '-': INIT_TOKEN(tok, TOKEN_MINUS, p->text + p->position, 1); break;
    case '/': INIT_TOKEN(tok, TOKEN_SLASH, p->text + p->position, 1); break;
    case '*': INIT_TOKEN(tok, TOKEN_ASTERISK, p->text + p->position, 1); break;
    case '(': INIT_TOKEN(tok, TOKEN_LEFT_BRACE, p->text + p->position, 1); break;
    case ')': INIT_TOKEN(tok, TOKEN_RIGHT_BRACE, p->text + p->position, 1); break;
    default: fprintf(stderr, "Unknown token\n");
    }
    ++p->position;
}

int main() {
    struct parser p;
    struct token t;

    parser_new(&p, "(13)2 + 123 * / -1 (");
    do {
        parser_lex(&p, &t);
        printf("%s: %.*s\n", KIND2STR[t.kind], t.len, t.start);
    } while(t.kind != TOKEN_EOF);
}